import Home from "./home";
import Login from "./login";
import Profile from "./profile"
import Playlists from "./playlists";
import Tracks from "./tracks";

export {Home, Login, Profile, Playlists, Tracks};
