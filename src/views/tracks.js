import React, {Fragment, useContext, useEffect, useState} from "react";
import {getTracks} from "../utils/spotifyAPI";
import Context from "../store/context";
import {Loading, Track} from "../components";

const TrackHeader = () => {
    return (
        <div>
            <div className="tracks_header">
                <div> # </div>
                <div> Track </div>
                <div> Album </div>
                <div> Duration </div>
            </div>
            <hr/>
        </div>
    )
}

const Tracks = (props) => {
    const {getGlobalToken, getUserId} = useContext(Context);
    const [isLoading, setIsLoading] = useState(false);
    const [tracks, setTracks] = useState([]);
    const playlist_id = props.match.params.playlist_id;

    useEffect(() => {
        if (getGlobalToken() && !isLoading) {
            setIsLoading(true);
            getTracks(getGlobalToken(), getUserId(), playlist_id).then( r => {
                if (r) {
                    for (var i = 0; i < r.data.items.length; i++)
                        tracks.push(r.data.items[i]);

                    setIsLoading(false);
                } else
                    alert("Get Tracks: error")
            })
        }
    }, [getGlobalToken()])

    return (
        <Fragment>
            { isLoading && <Loading /> }
            { !isLoading && tracks.length === 0 &&
                <span>This playlist has no tracks!</span> }
            { !isLoading && tracks.length !== 0 &&
                <div className="tracks">
                    <TrackHeader />
                    {tracks.map((t) => <Track key={t.id} track={t} />)}
                </div>
            }
        </Fragment>
    );
};

export default Tracks;
