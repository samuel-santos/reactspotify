import React, {Fragment, useContext, useEffect, useState} from "react";
import {getPlaylists} from "../utils/spotifyAPI";
import Context from "../store/context";
import {Card, CardDeck} from "react-bootstrap";
import {Loading} from "../components";
import {Link} from "react-router-dom";

const Playlists = () => {
    const {getGlobalToken, getUserId} = useContext(Context);
    const [isLoading, setIsLoading] = useState(false);
    const [playlists, setPlaylists] = useState([]);

    useEffect(() => {
        if (getGlobalToken() && !isLoading) {
            setIsLoading(true);
            getPlaylists(getGlobalToken(), getUserId()).then( r => {
                if (r) {
                    for (var i = 0; i < r.data.items.length; i++)
                        playlists.push(r.data.items[i]);

                    setIsLoading(false);
                } else
                    alert("Get Playlists: error")
            })
        }
    }, [getGlobalToken()])

    function getCardFromPlaylistItem(p) {
        let id = p.id
        let images = p.images;
        let name = p.name;
        let description = p.description;

        return (
            <Card key={id}>
                <Card.Img variant="top" src={images[0].url} />
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Text>
                        {description}
                    </Card.Text>
                </Card.Body>
                <Card.Footer>
                    <Link to={"/playlists/" + p.id + "/tracks"}>
                        <small className="text-muted">Go to tracks!</small>
                    </Link>
                </Card.Footer>
            </Card>
        );
    }

    return (
        <Fragment>
            { isLoading && <Loading /> }
            { !isLoading &&
                <CardDeck>
                    { playlists.length === 0 &&
                        <span>No playlists found!</span>
                    }
                    { playlists.length !== 0 && playlists.map((p) =>
                        getCardFromPlaylistItem(p)
                    )}
                </CardDeck>
            }
        </Fragment>
    );
};

export default Playlists;
