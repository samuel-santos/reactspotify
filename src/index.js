import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router } from "react-router-dom";
import useGlobalState from "./store/useGlobalState";
import Context from "./store/context";

const AppWrapper = () => {
    const store = useGlobalState();
    return (
        <Context.Provider value={store}>
            <Router>
                <App />
            </Router>
        </Context.Provider>
    );
}

ReactDOM.render(
    <AppWrapper />,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
