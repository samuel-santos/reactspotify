import React, {useState, useEffect, useContext} from 'react';
import './App.css';
import { Header, Loading } from "./components";
import { Home, Profile, Playlists, Tracks } from "./views";
import { Route, Switch, useHistory } from "react-router-dom";
import { Container } from "react-bootstrap";
import {getAuth, userHasLoggedInOnSpotify} from "./utils/spotifyAPI";
import Context from "./store/context";

function App() {
    const [isLoading, setIsLoading] = useState(false);
    const {getGlobalToken, handleGlobalToken, handleUserId} = useContext(Context);
    const history = useHistory();

    useEffect(() => {
        if (!isLoading && !getGlobalToken()) {
            if (userHasLoggedInOnSpotify()) {
                setIsLoading(true);
                getAuth().then(token => {
                    if (token) {
                        handleGlobalToken(token);
                        history.replace('/');
                        setIsLoading(false);
                    } else {
                        throw new Error();
                    }
                }).catch(e => {
                    setIsLoading(false);
                    alert("Login: Something went wrong!");
                });
            }
        }
    })

    function logout() {
        handleGlobalToken(null);
        handleUserId(null)
        history.push('/')
    }

    if (isLoading) {
        return <Loading />;
    }

    return (
        <div className="App">
          <Header logout={logout}/>
            <Container className="flex-grow-1 mt-5">
                <Switch>
                    <Route path="/" exact component={() => <Home />} />
                    <Route path="/profile" exact component={() => <Profile />} />
                    <Route path="/playlists" exact component={() => <Playlists />} />
                    <Route path="/playlists/:playlist_id/tracks" component={Tracks}/>
                </Switch>
            </Container>
        </div>
        );
    }

export default App;
