/**
 * This is an example of a basic node.js script that performs
 * the Authorization Code oAuth2 flow to authenticate against
 * the Spotify Accounts.
 *
 * For more information, read
 * https://developer.spotify.com/web-api/authorization-guide/#authorization_code_flow
 */

var querystring = require('querystring');

var client_id = '069ccebd71154fcb8d0a77d06da6a1c9'; // Your client id
var client_secret = '56138346b2214573b0ec144464a8219b'; // Your secret
var redirect_uri = 'http://localhost:3000/'; // Your redirect uri
const axios = require('axios');

var scope = 'user-read-private user-read-email playlist-read-private playlist-read-collaborative';

export const login_url = 'https://accounts.spotify.com/authorize?' +
    querystring.stringify({
        response_type: 'code',
        client_id: client_id,
        scope: scope,
        redirect_uri: redirect_uri,
    });

export const userHasLoggedInOnSpotify = () => {
    let params = window.location.search.split('&')[0].split("=");
    try {
        if (params != null && params[0].includes('code'))
            return true;
        else
            return false;
    } catch (Exception) {
        return false;
    }
}

export const getAuth = async () => {
    let code = window.location.search.split('&')[0].split("=")[1];

    const headers = {
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        auth: {
            username: client_id,
            password: client_secret,
        },
    };
    const data = {
        code: code,
        redirect_uri: redirect_uri,
        grant_type: 'authorization_code'
    };

    try {
        const response = await axios.post(
            'https://accounts.spotify.com/api/token',
            querystring.stringify(data),
            headers
        );
        console.log(response.data.access_token);
        return response.data.access_token;
    } catch (error) {
        return null;
    }
};

export const getMe = async (token) => {
    const headers = {
        headers: { 'Authorization': 'Bearer ' + token }
    };

    try {
        const response = await axios.get(
            'https://api.spotify.com/v1/me',
            headers
        );
        return response;
    } catch (error) {
        return null;
    }
};

export const getPlaylists = async (token, user_id) => {
    const headers = {
        headers: { 'Authorization': 'Bearer ' + token }
    };

    try {
        const response = await axios.get(
            'https://api.spotify.com/v1/users/' + user_id + '/playlists',
            headers
        );
        return response;
    } catch (error) {
        return null;
    }
};

export const getTracks = async (token, user_id, playlist_id) => {
    const headers = {
        headers: { 'Authorization': 'Bearer ' + token }
    };

    try {
        const response = await axios.get(
            'https://api.spotify.com/v1/users/' + user_id + '/playlists/' + playlist_id + '/tracks',
            headers
        );
        return response;
    } catch (error) {
        return null;
    }
};