import { useState } from 'react';

const useGlobalState = () => {
    const[globalToken, setGlobalToken] = useState(null);
    const[userId, setUserId] = useState(null);

    function handleGlobalToken(token) {
        localStorage.setItem('globalToken', token );
        setGlobalToken(token)
    }

    function getGlobalToken () {
        if (!globalToken) {
            let item = localStorage.getItem('globalToken');
            if (item === "null")
                return null
            return item;
        } else
            return globalToken;
    }

    function handleUserId(userId) {
        localStorage.setItem('userId', userId );
        setUserId(userId)
    }

    function getUserId() {
        if (!userId) {
            let item = localStorage.getItem('userId');
            setUserId(item);
            return item;
        } else
            return userId;
    }

    return { getGlobalToken, handleGlobalToken, getUserId, handleUserId }
}

export default useGlobalState;