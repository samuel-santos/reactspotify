import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlayCircle} from "@fortawesome/free-solid-svg-icons";

const Track = (props) => {
    const track = props.track

    function millisToMinutesAndSeconds(millis) {
        var minutes = Math.floor(millis / 60000);
        var seconds = ((millis % 60000) / 1000).toFixed(0);
        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }

    return (
        <div>
            <div className="track">
                <FontAwesomeIcon id="track_play" icon={faPlayCircle} />
                <div className="track_main">
                    <span id="track_name">{track.track.name}</span>
                    <span id="track_artists">
                        {track.track.artists
                            .map(a => a.name)
                            .reduce(
                                (list, name) => {list.push(name); return list;},
                                []
                            ).join(", ")
                        }
                    </span>
                </div>
                <span id="track_album">{track.track.album.name}</span>
                <span id="track_duration">{millisToMinutesAndSeconds(track.track.duration_ms)}</span>
            </div>
            <hr/>
        </div>
    );
};

export default Track;
