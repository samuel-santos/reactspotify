import React, {useContext, useEffect, useState} from "react";
import { NavLink as RouterNavLink, Link } from "react-router-dom";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import {getMe, login_url} from "../utils/spotifyAPI";
import Context from "../store/context";

const NavBar = (props) => {
    const [userName, setUserName] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const {getGlobalToken, handleUserId} = useContext(Context);
    const { logout } = props;

    useEffect(() => {
        if (getGlobalToken() && !isLoading) {
            setIsLoading(true)
            getMe(getGlobalToken()).then( r => {
                if (r) {
                    setUserName(r.data.display_name);
                    handleUserId(r.data.id);
                    setIsLoading(false);
                } else
                    alert("Get Me: error")
            })
        }
    }, [getGlobalToken()])

    return (
        <Navbar bg="dark" variant="dark">
            <Nav className="mr-auto">
                <Nav.Link
                    as={RouterNavLink}
                    to="/"
                    exact
                    activeClassName="router-link-exact-active"
                >
                    Home
                </Nav.Link>
            </Nav>
            {getNavbarOptions()}
        </Navbar>
    );

    function getNavbarOptions() {
        if (!getGlobalToken()) {
            return (
                <a href={login_url}>Login With Spotify</a>
            )
        } else {
            return (
                <NavDropdown title={"Welcome " + userName} id="basic-nav-dropdown">
                    <NavDropdown.Item>
                        <Link to='/profile' > Profile </Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                        <Link to='/playlists' > Playlists </Link>
                    </NavDropdown.Item>
                    <NavDropdown.Item>
                        <li onClick={logout} > Logout </li>
                    </NavDropdown.Item>
                </NavDropdown>
            )
        }
    }
};

export default NavBar;
